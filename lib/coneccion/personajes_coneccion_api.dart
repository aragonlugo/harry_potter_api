import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:harry_potter_api/modelo/personajes_api.dart';

class PersonajeService {
  Future<List<Personaje>> fetchPersonaje() async {
    List<Personaje> list = <Personaje>[];
    final uri = Uri.parse(
        "https://hp-api.onrender.com/api/characters"); // aqui se realiza la coneccion a la api
    http.Response response = await http.get(uri);

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      list = json.map<Personaje>((item) => Personaje.fromJson(item)).toList();
      list.sort((a, b) => a.name.compareTo(
          b.name)); // esto de aqui ordena alfabeticamente los personajes
      return list;
    } else
      return list;
  }
}
