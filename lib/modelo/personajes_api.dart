class Personaje {
  final String name;
  final String house;
  final bool isStudent;
  final String actor;
  final String image;

  Personaje(
      {required this.name,
      required this.house,
      required this.isStudent,
      required this.actor,
      required this.image});

  factory Personaje.fromJson(Map<String, dynamic> json) {
    return Personaje(
        actor: json["actor"],
        house: json["house"],
        image: json["image"].toString().replaceAll("herokuapp",
            "onrender"), //la api por defecto tiene los enlaces a herokuapp que se dio de baja, con esto se reemplaza por los enlaces de onrender
        isStudent: json["hogwartsStudent"],
        name: json["name"]);
  }
}
