import 'package:flutter/material.dart';
import 'package:harry_potter_api/modelo/personajes_api.dart';
import 'package:harry_potter_api/utilidades/funciones.dart';

class DetallesPersonajes extends StatefulWidget {
  final Personaje personaje;

  DetallesPersonajes(this.personaje);
  @override
  _DetallesPersonajesState createState() => _DetallesPersonajesState();
}

class _DetallesPersonajesState extends State<DetallesPersonajes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.personaje.name),
        backgroundColor: Colors.brown[800],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                child: widget.personaje.image == ""
                    ? Container(
                        margin: const EdgeInsets.only(top: 24),
                        //si no hay imagen disponible se muestra esta por defecto
                        child: Image.network(
                            "https://bitsofco.de/content/images/2018/12/broken-1.png"),
                      )
                    : Container(
                        margin: const EdgeInsets.only(top: 24),
                        //aqui se muestra la imagen del personaje si esta en la api
                        child: Image.network(widget.personaje.image),
                        height: 300,
                      )),
            Container(
              // aqui se muestra la faccion del personaje
              margin: const EdgeInsets.only(left: 8, right: 8),
              child: Card(
                child: Container(
                  margin: const EdgeInsets.only(left: 8, right: 8),
                  child: widget.personaje.house == ""
                      ? Container() // esto sirve para ocultar la "faccion" si no tiene una
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Casa: ",
                              style: infoStyle(),
                            ),
                            Text(
                              widget.personaje.house,
                              style: TextStyle(
                                  fontSize: 25,
                                  color: houseColor(widget.personaje.house)),
                            )
                          ],
                        ),
                ),
              ),
            ),
            Container(
              // aqui se muestra si estudia en hogwarts o no
              margin: const EdgeInsets.only(left: 8, right: 8),
              child: Card(
                child: Container(
                  margin: const EdgeInsets.only(left: 8, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Estudiante de Hogwarts: ",
                        style: infoStyle(),
                      ),
                      Text(
                        widget.personaje.isStudent.toString(),
                        style: TextStyle(fontSize: 25),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
                // aqui se muestra el nombre del actor si esta en la api
                margin: const EdgeInsets.only(left: 8, right: 8),
                child: Card(
                    child: Container(
                  margin: const EdgeInsets.only(left: 8, right: 8),
                  child: widget.personaje.house == ""
                      ? Container()
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Actuado por: ",
                              style: infoStyle(),
                            ),
                            Flexible(
                              //aqui acomoda automaticamente el nombre para evitar errores con nombres largos
                              child: Text(
                                widget.personaje.actor,
                                style: const TextStyle(fontSize: 25),
                              ),
                            )
                          ],
                        ),
                ))),
          ],
        ),
      ),
    );
  }

  TextStyle infoStyle() {
    return TextStyle(fontSize: 25);
  }
}
