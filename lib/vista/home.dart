import 'package:flutter/material.dart';
import 'package:harry_potter_api/coneccion/personajes_coneccion_api.dart';
import 'package:harry_potter_api/modelo/personajes_api.dart';
import 'package:harry_potter_api/utilidades/funciones.dart';
import 'package:harry_potter_api/vista/detalles_personajes.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  PersonajeService service = PersonajeService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Personajes"),
        backgroundColor: Color.fromARGB(255, 9, 39, 73),
      ),
      body: SingleChildScrollView(
          child: FutureBuilder<List<Personaje>>(
        future: service.fetchPersonaje(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
                children: snapshot.data!
                    .map((item) => Container(
                          margin:
                              const EdgeInsets.only(left: 8, right: 8, top: 8),
                          child: GestureDetector(
                            child: Card(
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 6, bottom: 6),
                                child: ListTile(
                                  title: Text(item.name),
                                  leading: Container(
                                    height: 50,
                                    width: 50,
                                    child: CircleAvatar(
                                      backgroundImage: NetworkImage(item.image),
                                    ),
                                  ),
                                  trailing: Text(
                                    item.house,
                                    style: TextStyle(
                                        color: houseColor(item.house)),
                                  ),
                                ),
                              ),
                            ),
                            onTap: () {
                              //al dar click se ingresa a la info personal de cada personaje
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          DetallesPersonajes(item)));
                            },
                          ),
                        ))
                    .toList());
          } else
            return Container(
                //esta parte sirve para centrar la representacion de carga al inciar la app
                margin: const EdgeInsets.only(top: 20),
                child: Center(child: CircularProgressIndicator()));
        },
      )),
    );
  }
}
